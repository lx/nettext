use err_derive::Error;

/// An error that happenned when creating a nettext encoder term
#[derive(Debug, Error)]
pub enum Error {
    #[error(display = "Invalid character '{}'", _0)]
    InvalidCharacter(u8),
    #[error(display = "Invalid RAW nettext litteral")]
    InvalidRaw,
    #[error(display = "Tried to insert into a term that isn't a dictionnary")]
    NotADictionnary,
    #[error(display = "Duplicate key: {}", _0)]
    DuplicateKey(String),
    #[error(
        display = "Refusing to build nested sequencess with seq(), use either seq_flatten() or seq_nested()"
    )]
    SeqInSeq,
}
